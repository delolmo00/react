
import './App.css';
import PrimerFragment from './componentes/PrimerFragment';
import { Variable } from './componentes/HolaMundo';
import { Button } from './componentes/Personalizados';
import BarraNav from './componentes/BarraNav';
import {Contador} from './componentes/ContadorUseState';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Reservas from './componentes/Reservas';
import React from "react";
import Deportes from './componentes/Deportes';
import Personajes from './componentes/Personajes';
import Timer from './componentes/UseEffect';


function App() {
  return (
    /*
<h1>Hola Mundo</h1>
    <PrimerFragment/>
    <Variable/>
    <Button/>
    <BarraNav/>
    <Contador/>
    
*/
    
    
    <>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<BarraNav />}></Route>
        <Route path="reservas" element={<Reservas />}></Route>
        <Route path="deportes" element={<Deportes />}></Route>
        <Route path="personajes" element={<Personajes />}></Route>
        <Route path="*" element={<p>error</p>}></Route>
        
      </Routes>
    </BrowserRouter>
<Timer></Timer>
  </>
    
    
    
    
  );
}

export default App;
