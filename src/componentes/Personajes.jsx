import BarraNav from "./BarraNav";

import { useState, useEffect } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';

import { Table } from 'react-bootstrap';

function Personajes() {

    const [personajes, setpersonajes] = useState([]);
    
    let tabla;
    useEffect(() => {
        axios.get('https://rickandmortyapi.com/api/character/[1,2,3,4,6,7]')
       // axios.get('https://rickandmortyapi.com/api/character/1,183')
            .then(response => {
                setpersonajes(response.data);
            });
    }, []);
    

    let listpersonajes = <h3>No hay personajes</h3>;
    if (personajes.length > 0) {

        
        listpersonajes = personajes.map(datos => (
            
            <tr>
                <td>{datos.name}</td>
                <td>{datos.gender}</td>
                <td><img src={datos.image} style={{width:50}}></img></td>
            </tr>
        ));
    }
    
        tabla = (
            <div>
        <Table striped bordered hover variant="dark">
            <thead>
                <tr>
                    <th className="bg-info">Nombre</th>
                    <th className="bg-info">Fecha</th>
                    <th className="bg-info">Foto</th>
                </tr>
            </thead>
            <tbody>
                {listpersonajes}
            </tbody>
        </Table>

    </div>

        )
        

    return (
        <>
        <BarraNav />
        {tabla}
        
        </>
    )

}



export default Personajes;