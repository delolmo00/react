import styled from 'styled-components';
import PropTypes from  'prop-types';
export const StyledButton = styled.button`
    padding: 10px 20px;
    color: black;
    border: 2px solid black;
    background-color: yellow;
    margin: 20px;
`;

export const StyledDiv = styled.div`
    width: 550px;
    height:200px;
    border: 2px solid black;
    background-color:yellow;
`;

export function Button({texto}) {
    return (
        <>
            <StyledButton >{texto}</StyledButton>
            <StyledDiv />
        </>
    );
}


Button.propTypes = {
    title:PropTypes.string,
    subTitle:PropTypes.number,
    texto: PropTypes.string,

}

Button.defaultProps = {
    title:'no hay titulo',
    subTitle: 'no hay titulo',
    texto: 'no hay titulo',
}
