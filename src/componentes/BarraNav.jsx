//import React, { useEffect, useState } from "react";

import "./BarraNav.css";
import { Button } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';


export default function BarraNav() {
 
  
    let buttons = (
      <div>
        
          
          <Button href="/reservas" className="ms-5 mt-3" variant="info">
          Reservas
          </Button>
          
          <Button href="/deportes" className="ms-5 mt-3" variant="info">
              Deportes
          </Button>
          <Button href="/personajes" className="ms-5 mt-3" variant="info">
              Personajes
          </Button>
          <Button href="#" className="ms-5 mt-3" variant="info">
              Contacto
          </Button>
          
          <Button href="#" className="ms-5 mt-3" variant="info">
            Registro
          </Button>
          <Button href="/" className="ms-5 mt-3" variant="danger">
              Salir
          </Button>
      </div>
      )
 

  return (
    <header>
      <nav>
        <ul>
          {buttons}
        </ul>
      </nav>
    </header>
  );
}
